from db_redis import r
import json



product_id = {
    'admin': '1234',
    'token': 'ghdsfvk',
    'MR_QUEUE': [],
    'MR_PASS_FLAG': False,
    'DISCUSSIONS': {},
}
# obj = {
#     'answer': 42,
#     'arr': [None, True, 3.14],
#     'truth': {
#         'coord': 'out there'
#     }
# }
# r.jsonset('2055', Path.rootPath(), product_id)

# r.hset('2055', 'admin', '123a4')
# r.hset('2055', 'token', 'ghdsfvk')
# r.hset('2055', 'MR_QUEUE', json.dumps([]))
# r.hset('2055', 'MR_PASS_FLAG', 0)
# r.hset('2055', 'DISCUSSIONS', json.dumps({}))

# MR_QUEUE = json.loads(r.hget('2055', 'MR_QUEUE'))
# MR_QUEUE.append('137')
# MR_QUEUE.append('147')
# r.hset('2055', 'MR_QUEUE', json.dumps(MR_QUEUE))
#
# r.hset_list_append('2055', 'MR_QUEUE', '999')

# res = r.hset_list_remove('2055', 'MR_QUEUE', '999')
res = r.hget_obj('2055', 'MR_QUEUE')
print(res)
res = r.hget_list_head('2055', 'MR_QUEUE')
print(res)


r.hset_dict_setattr('2055', 'DISCUSSIONS', '137', 'dsjckdckhcvbbe')
r.hset_dict_setattr('2055', 'DISCUSSIONS', '147', 'ncknvjfjnverjv')
res = r.hget_obj('2055', 'DISCUSSIONS')
print(res)
res = r.hget_dict_getattr('2055', 'DISCUSSIONS', '137')
print(res)

res = r.hgetall('2055')
print(res)
res = r.hget('2055', 'admin').decode()
if res == '123a4':
    print('ok')
print(res, type(res))
res = r.hget('2055', 'token').decode()
print(res, type(res))
res = json.loads(r.hget('2055', 'MR_QUEUE'))
print(res, type(res))
res = json.loads(r.hget('2055', 'MR_PASS_FLAG'))
print(res, type(res))
res = json.loads(r.hget('2055', 'DISCUSSIONS'))
print(res, type(res))
res = json.loads(r.hget('2055', 'DISCUSSIONS')).get('abc')
print(res, type(res))

res = r.hgetall('2055')
print(res)

