from flask import Flask, request

app = Flask(__name__)


# 跨域支持
def after_request(response):
    # JS前端跨域支持
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST'
    response.headers['Access-Control-Allow-Headers'] = 'x-requested-with,content-type'
    return response


@app.route("/api/v4/<pid>", methods=['GET', 'POST'])
def hello_world(pid):
    print(pid)
    print(request.form)
    print(request.data)
    print(request.json)
    return "<p>Hello, World!</p>"


if __name__ == '__main__':
    app.after_request(after_request)
    app.run('0.0.0.0', 8082)
